package az.com.expressbank.controller;


import az.com.expressbank.dto.ReqDto;
import az.com.expressbank.dto.RespDto;
import az.com.expressbank.service.CardService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/card")
@RequiredArgsConstructor
public class CardController {

    private final CardService cardService;

    @GetMapping("/check")
    public RespDto checkCardNumber(@RequestBody ReqDto reqDto) {
        return cardService.checkCardNumber(reqDto);
    }

}


