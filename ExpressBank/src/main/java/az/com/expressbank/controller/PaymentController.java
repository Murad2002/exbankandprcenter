package az.com.expressbank.controller;


import az.com.expressbank.dto.ReqDto;
import az.com.expressbank.dto.RespDto;
import az.com.expressbank.service.PaymentService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/payment")
@RequiredArgsConstructor
public class PaymentController {

    private final PaymentService paymentService;

    @PostMapping
    public RespDto doPayment(@RequestBody ReqDto reqDto){
        return paymentService.doPayment(reqDto);
    }

}


