package az.com.expressbank.domain;

import lombok.*;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity(name = "Card")
@ToString
public class Card {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    String cardNumber;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "customer_id")
    Customer customer;

    @OneToMany(mappedBy = "card")
    Set<Payment> payments;

    LocalDate expiryDate;

    @Column(nullable = false, updatable = false)
    @CreationTimestamp
    LocalDate insertDate;

    @Builder.Default
    @ColumnDefault(value = "1")
    Integer status = 1;

}
