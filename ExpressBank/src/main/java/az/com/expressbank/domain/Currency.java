package az.com.expressbank.domain;

import lombok.*;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity(name = "Currency")
public class Currency {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    String currencyName;

    Float converter;

    @OneToMany(mappedBy = "currency")
    Set<Payment> payments;

    @Column(nullable = false, updatable = false)
    @CreationTimestamp
    LocalDate insertDate;

    @Builder.Default
    @ColumnDefault(value = "1")
    Integer status = 1;

}
