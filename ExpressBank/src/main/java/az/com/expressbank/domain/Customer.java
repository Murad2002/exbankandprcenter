package az.com.expressbank.domain;

import lombok.*;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity(name = "Customer")
@ToString
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    String name;

    String surname;

    LocalDate birthdate;

    @OneToMany(mappedBy = "customer")
    Set<Card> cards;

    @Column(nullable = false, updatable = false)
    @CreationTimestamp
    LocalDate insertDate;

    @Builder.Default
    @ColumnDefault(value = "1")
    Integer status = 1;
}
