package az.com.expressbank.domain;

import lombok.*;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity(name = "Payment")
public class Payment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @ManyToOne
    @JoinColumn(name = "card_id")
    Card card;

    Float amount;
    @ManyToOne
    @JoinColumn(name = "currency_id")
    Currency currency;

    @Column(nullable = false, updatable = false)
    @CreationTimestamp
    LocalDate insertDate;

    @Builder.Default
    @ColumnDefault(value = "1")
    Integer status = 1;
}
