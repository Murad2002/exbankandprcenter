package az.com.expressbank.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;

import java.time.LocalDate;
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Getter
@Setter
public class ReqDto {

    String cardNumber;

    LocalDate birthDate;

    Float amount;

    Long currencyId;
}
