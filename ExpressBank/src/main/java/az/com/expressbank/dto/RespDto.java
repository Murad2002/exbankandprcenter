package az.com.expressbank.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Getter
@Setter
public class RespDto {

    Integer statusCode;

    String customerName;

    String customerSurName;

    String cardNumber;

    LocalDateTime dateTime;

    String message;
}
