package az.com.expressbank.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public enum Response {

    INVALID_REQUEST_DATA(400,"Bad Request !"),
    CARD_NOT_FOUND(425,"Card not found !"),
    CARD_ACTIVE(802,"Card is active !"),
    CARD_IS_EXPIRED(803,"Car is expired !"),
    INTERNAL_SERVER_ERROR(500,"Server problem !");


    private final int statusCode;
    private final String message;


}

