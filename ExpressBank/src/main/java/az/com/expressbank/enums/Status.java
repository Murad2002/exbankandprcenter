package az.com.expressbank.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public enum Status {

    ACTIVE(1),
    DEACTIVE(0);

    private final int value;



}
