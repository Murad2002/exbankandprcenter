package az.com.expressbank.exception;

import az.com.expressbank.dto.RespDto;
import az.com.expressbank.enums.Response;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;

@ControllerAdvice
public class CustomizedExceptionHandling extends ResponseEntityExceptionHandler {

    @ExceptionHandler(InvalidRequestException.class)
    public ResponseEntity<Object> invalidRequestHandling(InvalidRequestException exception, WebRequest webRequest) {
        ErrorResponse response = ErrorResponse.builder()
                .statusCode(Response.INVALID_REQUEST_DATA.getStatusCode())
                .dateTime(LocalDateTime.now())
                .message(Response.INVALID_REQUEST_DATA.getMessage())
                .build();
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<Object> notFoundHandling(NotFoundException exception, WebRequest webRequest) {
        ErrorResponse response = ErrorResponse.builder()
                .statusCode(Response.CARD_NOT_FOUND.getStatusCode())
                .dateTime(LocalDateTime.now())
                .message(Response.CARD_NOT_FOUND.getMessage())
                .build();
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

}
