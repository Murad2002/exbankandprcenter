package az.com.expressbank.repository;

import az.com.expressbank.domain.Card;
import az.com.expressbank.domain.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CardRepository extends JpaRepository<Card,Long> {

    Card findByCardNumberAndCustomerAndStatus(String cardNumber, Customer customer, Integer status);

    Card findByCardNumberAndStatus(String cardNumber, int value);
}
