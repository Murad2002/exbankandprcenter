package az.com.expressbank.repository;

import az.com.expressbank.domain.Card;
import az.com.expressbank.domain.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;

@Repository
public interface CustomerRepository extends JpaRepository<Customer,Long> {

    Customer findByBirthdateAndStatus(LocalDate birtDate, Integer status);
}
