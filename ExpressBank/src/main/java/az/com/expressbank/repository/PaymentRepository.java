package az.com.expressbank.repository;

import az.com.expressbank.domain.Card;
import az.com.expressbank.domain.Payment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PaymentRepository extends JpaRepository<Payment,Long> {
}
