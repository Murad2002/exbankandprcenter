package az.com.expressbank.service;

import az.com.expressbank.domain.Card;
import az.com.expressbank.domain.Customer;
import az.com.expressbank.dto.ReqDto;
import az.com.expressbank.dto.RespDto;
import az.com.expressbank.enums.Status;
import az.com.expressbank.exception.InvalidRequestException;
import az.com.expressbank.exception.NotFoundException;
import az.com.expressbank.repository.CardRepository;
import az.com.expressbank.repository.CustomerRepository;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.concurrent.CompletableFuture;

@Service
@RequiredArgsConstructor
public class CardService {

    private final CardRepository cardRepository;
    private final CustomerRepository customerRepository;
    private final RestTemplate restTemplate;

    public RespDto checkCardNumber(ReqDto reqDto) {
        RespDto response = new RespDto();
        if (reqDto.getBirthDate() == null || reqDto.getCardNumber() == null) {
            throw new InvalidRequestException();
        }

        Customer customer = customerRepository.findByBirthdateAndStatus(reqDto.getBirthDate(), Status.ACTIVE.getValue());
        if (customer == null) {
            throw new NotFoundException();
        }
        Card card = cardRepository.findByCardNumberAndCustomerAndStatus(reqDto.getCardNumber(), customer, Status.ACTIVE.getValue());

        if (card == null) {
            throw new NotFoundException();
        }

        response = checkFromProcessingCenter(reqDto.getCardNumber());
        response.setDateTime(LocalDateTime.now());
        response.setCustomerName(customer.getName());
        response.setCustomerSurName(customer.getSurname());
        response.setCardNumber(card.getCardNumber());

        return response;
    }

    public RespDto checkFromProcessingCenter(String cardNumber) {

        String url = "http://localhost:8083/v1/card/check/" + cardNumber;
        return sendGetRequestWithRestTemplate(url);
    }

    public RespDto sendGetRequestWithRestTemplate(String url) {
        return restTemplate.getForObject(url, RespDto.class);
    }

}



