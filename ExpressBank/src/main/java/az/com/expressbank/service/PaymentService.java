package az.com.expressbank.service;

import az.com.expressbank.domain.Card;
import az.com.expressbank.domain.Currency;
import az.com.expressbank.domain.Payment;
import az.com.expressbank.dto.ProcessingCenterReqDto;
import az.com.expressbank.dto.ReqDto;
import az.com.expressbank.dto.RespDto;
import az.com.expressbank.enums.Status;
import az.com.expressbank.exception.InvalidRequestException;
import az.com.expressbank.exception.NotFoundException;
import az.com.expressbank.repository.CardRepository;
import az.com.expressbank.repository.CurrencyRepository;
import az.com.expressbank.repository.PaymentRepository;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.concurrent.CompletableFuture;

@Service
@RequiredArgsConstructor
@Slf4j
public class PaymentService {

    private final PaymentRepository paymentRepository;
    private final CurrencyRepository currencyRepository;
    private final CardRepository cardRepository;
    private final RestTemplate restTemplate;

    public RespDto doPayment(ReqDto reqDto) {

        if (reqDto.getCardNumber() == null || reqDto.getAmount() == null || reqDto.getCurrencyId() == null) {
            throw new InvalidRequestException();
        }

        Currency currency = currencyRepository.findById(reqDto.getCurrencyId()).orElseThrow(InvalidRequestException::new);

        Card card = cardRepository.findByCardNumberAndStatus(reqDto.getCardNumber(), Status.ACTIVE.getValue());
        RespDto response = updateBalance(reqDto.getCardNumber(), reqDto.getAmount() * currency.getConverter());
        response.setCustomerName(card.getCustomer().getName());
        response.setCustomerSurName(card.getCustomer().getSurname());
        response.setCardNumber(card.getCardNumber());
        response.setDateTime(LocalDateTime.now());

        Payment payment = Payment.builder()
                .card(card)
                .amount(reqDto.getAmount())
                .build();
        paymentRepository.save(payment);
        return response;
    }

    private RespDto updateBalance(String cardNumber, float amount) {
        String url = "http://localhost:8083/v1/card/update";
        ProcessingCenterReqDto reqDto = ProcessingCenterReqDto.builder()
                .cardNumber(cardNumber)
                .amount(amount)
                .build();
        return sendPostRequestWithRestTemplate(url, reqDto);
    }

    public RespDto sendPostRequestWithRestTemplate(String url, ProcessingCenterReqDto reqDto) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        HttpEntity<ProcessingCenterReqDto> entity = new HttpEntity<>(reqDto, headers);
        return restTemplate.postForObject(url, entity, RespDto.class);
    }

//    restTemplate.exchange(url,HttpMethod.POST,entity,RespDto.class).getBody();

}
