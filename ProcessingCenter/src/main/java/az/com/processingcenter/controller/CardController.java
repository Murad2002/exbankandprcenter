package az.com.processingcenter.controller;


import az.com.processingcenter.dto.ReqDto;
import az.com.processingcenter.dto.RespDto;
import az.com.processingcenter.exception.CardExpiredException;
import az.com.processingcenter.exception.CardNotActiveException;
import az.com.processingcenter.service.CardService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/card")
@RequiredArgsConstructor
public class CardController {

    private final CardService cardService;

    @GetMapping("/check/{cardNumber}")
    public RespDto checkCardStatus(@PathVariable String cardNumber) {
        return cardService.checkCardStatus(cardNumber);
    }

    @PostMapping("/update")
    public RespDto updateBalance(@RequestBody ReqDto reqDto){
        return cardService.updateBalance(reqDto);
    }



}


