package az.com.processingcenter.domain;

import az.com.processingcenter.enums.Response;
import lombok.*;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity(name = "Card")
public class Card {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    String cardNumber;

    Float balance;

    LocalDate expiryDate;

    @Column(nullable = false, updatable = false)
    @CreationTimestamp
    LocalDate insertDate;

    @ColumnDefault(value = "802")
    Integer status;

}
