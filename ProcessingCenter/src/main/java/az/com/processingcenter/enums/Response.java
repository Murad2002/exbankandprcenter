package az.com.processingcenter.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public enum Response {

    CARD_NOT_FOUND(800,"Card not found !"),
    CARD_NOT_ACTIVE(801,"Card is not active !"),
    CARD_ACTIVE(802,"Card is active !"),
    CARD_IS_EXPIRED(803,"Car is expired !"),
    INTERNAL_SERVER_ERROR(500,"Server problem !");

    private final int statusCode;
    private final String message;


}

