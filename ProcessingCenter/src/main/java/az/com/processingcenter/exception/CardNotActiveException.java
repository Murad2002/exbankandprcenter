package az.com.processingcenter.exception;

public class CardNotActiveException extends RuntimeException {
    public CardNotActiveException(String message) {
        super(message);
    }

    public CardNotActiveException() {

    }

}
