package az.com.processingcenter.exception;

import az.com.processingcenter.dto.RespDto;
import az.com.processingcenter.enums.Response;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;

@ControllerAdvice
public class CustomizedExceptionHandling extends ResponseEntityExceptionHandler {

    @ExceptionHandler(CardNotFoundException.class)
    public ResponseEntity<Object> handleCardNotFoundException(CardNotFoundException exception, WebRequest webRequest) {
        RespDto response = RespDto.builder()
                .statusCode(Response.CARD_NOT_FOUND.getStatusCode())
                .dateTime(LocalDateTime.now())
                .message(Response.CARD_NOT_FOUND.getMessage())
                .build();
        return new ResponseEntity<>(response,HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(CardNotActiveException.class)
    public ResponseEntity<Object> handleCardNotActiveException(CardNotActiveException exception, WebRequest webRequest) {
        RespDto response = RespDto.builder()
                .statusCode(Response.CARD_NOT_ACTIVE.getStatusCode())
                .dateTime(LocalDateTime.now())
                .message(Response.CARD_NOT_ACTIVE.getMessage())
                .build();
        return new ResponseEntity<>(response,HttpStatus.CONFLICT);
    }

    @ExceptionHandler(CardExpiredException.class)
    public ResponseEntity<Object> handleCardExpiredException(CardExpiredException exception, WebRequest webRequest) {
        RespDto response = RespDto.builder()
                .statusCode(Response.CARD_IS_EXPIRED.getStatusCode())
                .dateTime(LocalDateTime.now())
                .message(Response.CARD_IS_EXPIRED.getMessage())
                .build();
        return new ResponseEntity<>(response, HttpStatus.CONFLICT);
    }

}
