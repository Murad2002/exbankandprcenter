package az.com.processingcenter.repository;

import az.com.processingcenter.domain.Card;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CardRepository extends JpaRepository<Card,Long> {

    Card findByCardNumberAndStatus(String cardNumber , Integer status);
}
