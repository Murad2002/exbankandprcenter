package az.com.processingcenter.service;

import az.com.processingcenter.domain.Card;
import az.com.processingcenter.dto.ReqDto;
import az.com.processingcenter.dto.RespDto;
import az.com.processingcenter.enums.Response;
import az.com.processingcenter.exception.CardExpiredException;
import az.com.processingcenter.exception.CardNotActiveException;
import az.com.processingcenter.exception.CardNotFoundException;
import az.com.processingcenter.repository.CardRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CardService {

    private final CardRepository cardRepository;

    public RespDto checkCardStatus(String cardNumber) {
        Card card = cardRepository.findByCardNumberAndStatus(cardNumber, Response.CARD_ACTIVE.getStatusCode());
        if (card == null) {
            throw new CardNotFoundException();
        }
        if (card.getStatus() == Response.CARD_IS_EXPIRED.getStatusCode()) {
            throw new CardExpiredException();
        }
        if (card.getStatus() == Response.CARD_NOT_ACTIVE.getStatusCode()) {
            throw new CardNotActiveException();
        }

        RespDto response = RespDto.builder()
                .statusCode(200)
                .message("Card is Active !")
                .build();

        return response;
    }


    public RespDto updateBalance(ReqDto reqDto) {

        Card card = cardRepository.findByCardNumberAndStatus(reqDto.getCardNumber(), Response.CARD_ACTIVE.getStatusCode());
        if (card == null) {
            throw new CardNotFoundException();
        }
        if (card.getStatus() == Response.CARD_IS_EXPIRED.getStatusCode()) {
            throw new CardExpiredException();
        }
        if (card.getStatus() == Response.CARD_NOT_ACTIVE.getStatusCode()) {
            throw new CardNotActiveException();
        }
        System.out.println("hello");
        card.setBalance(card.getBalance() + reqDto.getAmount());
        cardRepository.save(card);
        RespDto response = RespDto.builder()
                .statusCode(200)
                .message("Successfully Updated !")
                .build();
        return response;
    }
}
